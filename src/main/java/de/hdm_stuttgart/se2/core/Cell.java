package de.hdm_stuttgart.se2.core;

import de.hdm_stuttgart.se2.interfaces_abstracts.IFieldObject;

public class Cell {

    IFieldObject fieldObject;

    public IFieldObject getFieldObject(){return this.fieldObject;}

    Cell(IFieldObject emptyField) {
        this.fieldObject = emptyField;
    }

    void setPlayer(IFieldObject player) {
        this.fieldObject = player;
    }

}