package de.hdm_stuttgart.se2.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GameLogic {

    private Logger log = LogManager.getLogger("logfile");

    private static GameLogic gameLogic;
    private GameBoard gameBoard = GameBoard.getGameBoard();
    private volatile boolean  gameRunning;

    /**
     * Singleton für die GameLogic. Erzeugt eine GameLogic, wenn es noch keine gibt. Sonst gibt es die existierende zurück.
     *
     * @return existierende GameLogic
     */
    public static GameLogic getGameLogic() {
        if (gameLogic == null) {
            gameLogic = new GameLogic();
            return gameLogic;
        } else {
            return gameLogic;
        }
    }

    /**
     * boolean won für Gewinnbedingungen
     */
    private boolean won;

    /**
     * Gibt den boolean-Wert für won zurück
     * @return boolean won
     */
    public boolean getWon(){
        return this.won;
    }

    /**
     * Um won bei Restart wieder auf false setzen zu können
     * @param won boolean won für Gewinnbedingungen
     */
    public void setWon(boolean won) {
        this.won = won;
    }

    /**
     * checkWin-Methode bringt alle einzelnen Gewinnbedingungs-Methoden zusammen
     */
    public void checkWin() {
        checkHorizontalWin();
        checkVerticalWin();
        checkDiagonalLeftUpWin();
        checkDiagonalRightUpWin();
    }

    /**
     * Gewinnbedingungen und deren Prüfung in beide Richtungen erstellen:
     * <p>
     * Horizontale Gewinnabfrage:
     */

    private void checkHorizontalWin() {
        int counter = 0;
        int checkID = gameBoard.getCellArray()[gameBoard.getX()][gameBoard.getY()].fieldObject.getId();
        for (int i = gameBoard.getX(); i >= 0; i--) {
            if (checkID == gameBoard.getCellArray()[i][gameBoard.getY()].fieldObject.getId()) {
                counter++;
            } else break;
        }

        for (int i = gameBoard.getX(); i < gameBoard.getWidth(); i++) {
            if (checkID == gameBoard.getCellArray()[i][gameBoard.getY()].fieldObject.getId()) {
                counter++;
            } else break;
        }

        if (counter >= 5) {
            log.info(PlayerList.getPlayerList().getPlayerName(checkID) + " hat gewonnen");
            this.won = true;
        }


    }

    /**
     * Vertikale Gewinnabfrage:
     */
    private void checkVerticalWin() {
        int counter = 0;
        int checkID = gameBoard.getCellArray()[gameBoard.getX()][gameBoard.getY()].fieldObject.getId();
        for (int i = gameBoard.getY(); i >= 0; i--) {
            if (checkID == gameBoard.getCellArray()[gameBoard.getX()][i].fieldObject.getId()) {
                counter++;
            } else break;
        }

        for (int i = gameBoard.getY(); i < gameBoard.getHeight(); i++) {
            if (checkID == gameBoard.getCellArray()[gameBoard.getX()][i].fieldObject.getId()) {
                counter++;
            } else break;
        }

        if (counter >= 5) {
            log.trace(PlayerList.getPlayerList().getPlayerName(checkID) + " hat gewonnen");
            this.won = true;
        }

    }

    /**
     * Diagonal-Rechts-Gewinnabfrage:
     */
    private void checkDiagonalRightUpWin() {
        int counter = 0;
        int vertOffset = 0; // Bewegung auf der Y-Achse
        int checkID = gameBoard.getCellArray()[gameBoard.getX()][gameBoard.getY()].fieldObject.getId(); // Farbe des aktuellen Coins

        for (int i = gameBoard.getX(); i >= 0; i--) { // Prüfung, dass man das Gameboard nicht nach links verlässt
            if (gameBoard.getY() + vertOffset < gameBoard.getHeight()) { // Prüfung, dass man das Gameboard nicht nach unten verlässt
                if (checkID == gameBoard.getCellArray()[i][gameBoard.getY() + vertOffset].fieldObject.getId()) {
                    counter++; // Da ein Coin des selben Spielers gefunden wurde wird der counter um eins erhöht
                    vertOffset++; // Beim nächsten Durchlauf ein Feld weiter unten prüfen
                } else break; // Prüfung nach unten links beenden, da ein Coin eines anderen Spielers gefunden wurde
            } else break; // Prüfung nach links unten beenden, da das untere Ende des Gameboard erreicht ist
        }

        vertOffset = 0; // Bewegung auf der Y-Achse zurücksetzen

        for (int i = gameBoard.getX(); i < gameBoard.getWidth(); i++) { // Prüfung, dass man das Gameboard nicht nach rechts verlässt
            if (gameBoard.getY() + vertOffset >= 0) { // Prüfung, dass man das Gameboard nicht nach oben verlässt
                if (checkID == gameBoard.getCellArray()[i][gameBoard.getY() + vertOffset].fieldObject.getId()) {
                    counter++; // Da ein Coin des selben Spielers gefunden wurde wird der counter um eins erhöht
                    vertOffset--; // Beim nächsten Durchlauf ein Feld weiter oben prüfen
                } else break; // Prüfung nach oben rechts beenden, da ein Coin eines anderen Spielers gefunden wurde
            } else break; // Prüfung nach rechts oben beenden, da das obere Ende des Gameboard erreicht ist
        }



        if (counter >= 5) {
            log.trace(PlayerList.getPlayerList().getPlayerName(checkID) + " hat gewonnen");
            this.won = true;
        }
    }

    /**
     * Diagonal-Links-Gewinnabfrage:
     */
    private void checkDiagonalLeftUpWin() {
        int counter = 0;
        int vertOffset = 0; // Bewegung auf der Y-Achse
        int checkID = gameBoard.getCellArray()[gameBoard.getX()][gameBoard.getY()].fieldObject.getId(); // Farbe des aktuellen Coins

        for (int i = gameBoard.getX(); i >= 0; i--) { // Prüfung, dass man das Gameboard nicht nach links verlässt
            if (gameBoard.getY() + vertOffset >= 0) { // Prüfung, dass man das Gameboard nicht nach oben verlässt
                if (checkID == gameBoard.getCellArray()[i][gameBoard.getY() + vertOffset].fieldObject.getId()) {
                    counter++; // Da ein Coin des selben Spielers gefunden wurde wird der counter um eins erhöht
                    vertOffset--; // Beim nächsten Durchlauf ein Feld weiter oben prüfen
                } else break; // Prüfung nach oben links beenden, da ein Coin eines anderen Spielers gefunden wurde
            } else break; // Prüfung nach links oben beenden, da das obere Ende des Gameboard erreicht ist
        }

        vertOffset = 0; // Bewegung auf der Y-Achse zurücksetzen

        for (int i = gameBoard.getX(); i < gameBoard.getWidth(); i++) { // Prüfung, dass man das Gameboard nicht nach rechts verlässt
            if (gameBoard.getY() + vertOffset < gameBoard.getHeight()) { // Prüfung, dass man das Gameboard nicht nach unten verlässt
                if (checkID == gameBoard.getCellArray()[i][gameBoard.getY() + vertOffset].fieldObject.getId()) {
                    counter++; // Da ein Coin des selben Spielers gefunden wurde wird der counter um eins erhöht
                    vertOffset++; // Beim nächsten Durchlauf ein Feld weiter unten prüfen
                } else break; // Prüfung nach unten rechts beenden, da ein Coin eines anderen Spielers gefunden wurde
            } else break; // Prüfung nach rechts unten beenden, da das untere Ende des Gameboard erreicht ist
        }

        if (counter >= 5) {
            log.trace(PlayerList.getPlayerList().getPlayerName(checkID) + " hat gewonnen");
            this.won = true;
        }
    }

    public boolean isGameRunning() {
        return gameRunning;
    }

    public void setGameRunning(boolean running) {
        gameRunning = running;
        log.debug("GameRunnung = " + running);
    }
}