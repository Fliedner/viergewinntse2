package de.hdm_stuttgart.se2.fxmlGui;

import de.hdm_stuttgart.se2.Exceptions.FXMLLoaderException;
import de.hdm_stuttgart.se2.threads.PrintGameboardToFile;
import de.hdm_stuttgart.se2.threads.PrintPlayerNamesToFile;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class StartScreenController {

    private AlertBoxController alertBoxController = new AlertBoxController();
    private Logger log = LogManager.getLogger("logfile");
    private static PrintGameboardToFile thread_printGameboardToFile = PrintGameboardToFile.getPrintGameboard_toFile();
    private static PrintPlayerNamesToFile thread_printPlayerNamesToFile = PrintPlayerNamesToFile.getPrintPlayerNamesToFile();

    @FXML
    public void initialize() {
        thread_printGameboardToFile.start();
        thread_printPlayerNamesToFile.start();

    }

    /**
     * Führt zu Alert-Screen ob Spiel wirklich beendet werden soll, falls ja wird es beendet
     * @param event Beenden-Button
     */
    public void Beenden(ActionEvent event) {
        if (alertBoxController.beendenButton()) System.exit(0);
    }

    /**
     * Führt zum Name-Screen um Spiel zu starten
     * @param event Start-Button
     * @throws FXMLLoaderException beim Laden des FXMLLoaders
     */
    public void StartButtonClicked(ActionEvent event) throws IOException {
        log.debug("Wechsel zu NameScreen.");
        try {
            Parent startParent = FXMLLoader.load(getClass().getResource("/NameScreen.fxml"));
            Scene GoScene = new Scene(startParent);
            Stage window2 = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window2.setScene(GoScene);
            window2.show();
        } catch (LoadException Le){
            alertBoxController.FXMLExceptionThrown("'NameScreen.fxml'");
            log.fatal("FXMLLoaderException: FXML-Datei 'NameScreen.fxml' ist fehlerhaft.");
            throw new FXMLLoaderException("Die FXML Datei 'NameScreen.fxml' konnte nicht geladen werden!");
        } catch (IOException Ie) {
            alertBoxController.IOExceptionThrown();
            log.fatal("Bei Wechsel zu NameScreen ist eine IOException aufgetreten!");
        }
    }
}



