package de.hdm_stuttgart.se2.fxmlGui;

import de.hdm_stuttgart.se2.Exceptions.FXMLLoaderException;
import de.hdm_stuttgart.se2.core.PlayerList;
import de.hdm_stuttgart.se2.fieldObjects.FieldObjectFactory;
import de.hdm_stuttgart.se2.interfaces_abstracts.IFieldObject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class NameScreenController {

    private AlertBoxController alertBoxController = new AlertBoxController();
    private Logger log = LogManager.getLogger("logfile");

    @FXML
    public TextField NameSpieler2;
    public TextField NameSpieler1;


    @FXML
    public void setNameSpieler1() {
    }

    @FXML
    public void setNameSpieler2() {
    }

    /**
     * Namenseingabe von Spieler1 und Spieler2, Alert-Fenster und Szenenwechsel zu GameScreen
     *
     * @param event Start Button drücken und damit Nameneingaben bestätigen
     * @throws FXMLLoaderException bei aufkommenden Fehlern beim Laden des FXMLLoaders
     */
    @FXML
    public void submitName(ActionEvent event) throws IOException {

        try {
            String text1 = NameSpieler1.getText();
            String text2 = NameSpieler2.getText();

            /*
              Kontrolle ob Textfelder leer sind, falls leer -> Alert-Fenster

             */
            if (!alertBoxController.fehlerKeinName(text1, text2)) {
                Parent startParent = FXMLLoader.load(getClass().getResource("/NameScreen.fxml"));
                Scene GoScene = new Scene(startParent);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(GoScene);
                window.show();

            } else if (!alertBoxController.fehlerGleicherName(text1, text2)) {
                Parent startParent = FXMLLoader.load(getClass().getResource("/NameScreen.fxml"));
                Scene GoScene = new Scene(startParent);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(GoScene);
                window.show();
            } else {

                log.debug("Spiel starten.");
                /*
                  Name v. Spieler1 und Spieler2 eingeben
                 */
                PlayerList.getPlayerList().addPlayer(this.createPlayer(1, text1, Color.RED));
                //System.out.println(PlayerList.getPlayerList().getPlayerName(1));
                PlayerList.getPlayerList().addPlayer(this.createPlayer(2, text2, Color.YELLOW));
                //System.out.println(PlayerList.getPlayerList().getPlayerName(2));

                /*
                 * Neues Fenster mit GameScreen öffnen
                 */
                Parent startParent = FXMLLoader.load(getClass().getResource("/GameScreen.fxml"));
                Scene GoScene = new Scene(startParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(GoScene);
                window.show();
            }
        } catch (LoadException Le){
            alertBoxController.FXMLExceptionThrown("'GameScreen.fxml'");
            log.fatal("FXMLLoaderException: FXML-Datei 'GameScreen.fxml' ist fehlerhaft.");
            throw new FXMLLoaderException("Die FXML Datei 'GameScreen.fxml' konnte nicht geladen werden!");
        } catch (IOException Ie) {
            alertBoxController.IOExceptionThrown();
            log.fatal("Bei Wechsel zu GameScreen ist eine IOException aufgetreten!");
        }
    }

    /**
     * Methode, die Factory aufruft um neuen Player zu generieren
     *
     * @param playerId ID 1 oder 2 für Spieler1 und Spieler2
     * @param name     Name der Spieler als String, ID zugeordnet
     * @param color    Farbe des Spielers, ID zugeordnet
     * @return Spieler bzw Player als Objekt
     */
    private IFieldObject createPlayer(int playerId, String name, Color color) {
        return FieldObjectFactory.getFactory().createFieldObject(playerId, name, color);
    }
}
