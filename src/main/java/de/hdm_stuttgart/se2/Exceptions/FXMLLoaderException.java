package de.hdm_stuttgart.se2.Exceptions;

import javafx.fxml.LoadException;

public class FXMLLoaderException extends LoadException {

    public FXMLLoaderException(){

    }

    public FXMLLoaderException(String message){
        super(message);
    }

}
